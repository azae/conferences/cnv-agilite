Les documents : 
* [support Agile France 2017](https://gitlab.com/azae/conferences/cnv-agilite/builds/artifacts/master/file/2017agileFrance.pdf?job=compile_pdf)
* [support Agile Rennes 2017](https://gitlab.com/azae/conferences/cnv-agilite/builds/artifacts/master/file/2017AgileRennes.pdf?job=compile_pdf)
* [Les critères d'acceptance](https://gitlab.com/azae/conferences/cnv-agilite/builds/artifacts/master/file/criteresAcceptance.pdf?job=compile_pdf)

Pour la prochaine fois 
* changer la mise en forme des examples : plus gors, plus de mise en évidence des changements.
